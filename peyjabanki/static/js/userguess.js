$(function() {
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  }

  function displayAjaxResult(data, row){
    if (data.success === true){
      row.find('.alertSuccess').fadeIn();
      setTimeout(function(){
        row.find('.alertSuccess').fadeOut();
      }, 1000);
    }else{
      row.find('.alertError').fadeIn();
      setTimeout(function(){
        row.find('.alertError').fadeOut();
      }, 1000);
    }
  }
  var csrftoken = getCookie('csrftoken');

  $.ajaxSetup({
    headers: { "X-CSRFToken": csrftoken }
  });
  $('.awayscore').on('blur', function(){

    var row = $(this).parent().parent().parent();
    var data = {
      'action': 'game',
      'awayscore': $(this).val(),
      'game': row.data('game'),
      'homescore': row.find('.homescore').val()
    }

    $.ajax({
      url: document.location.href,
      type: 'POST',
      dataType: 'json',
      data: data,
    })
    .done(function(data) {
      displayAjaxResult(data, row);
    })
    .fail(function(data) {
      console.log(data);
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });

  $('.extra').on('change', function(){
    $this = $(this);
    var data = {
      'guess': $this.find(':selected').data('choice'),
      'extra': $this.attr('id'),
      'action': 'extra'
    };

    $.ajax({
      url: document.location.href,
      type: 'POST',
      dataType: 'json',
      data: data,
    })
    .done(function(data) {
      displayAjaxResult(data, $this.parent().parent().parent());
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });
});
