import operator

from django.utils import timezone

from django.db.models import Sum, Prefetch

from django.http import JsonResponse, HttpResponse

from django.views.generic import (
    ListView,
    TemplateView
)

from django.contrib.auth.models import User

from .forms import UserGuessFormSet
from .models import Competition, UserGuess, Game, BonusQuestion, UserBonusAnswer, BonusQuestionChoices


class ScoreView(ListView):
    template_name = 'scores.html'

    model = User

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        userscores = {u.pk: u for u in User.objects.filter(
            is_superuser=False, is_active=True
        ).annotate(
            score=Sum('userguess__points')
        )}

        extrascores = {u.pk: u for u in User.objects.filter(
            is_superuser=False
        ).annotate(
            extra=Sum('bonusguess__points')
        )}

        res = {}

        for pk, user in userscores.items():
            userscore = user.score if user.score is not None else 0
            extrascore = extrascores[pk].extra if extrascores[pk].extra else 0
            res[user.username] = userscore + extrascore
        sorted_x = sorted(res.items(), key=operator.itemgetter(1), reverse=True)
        context['userscores'] = sorted_x

        return context


class GuessView(TemplateView):

    template_name = 'userguess.html'
    model = User
    form_class = UserGuessFormSet

    def post(self, request, **kwargs):
        if hasattr(self, request.POST.get('action', '')):
            response = getattr(self, request.POST.get('action'))(request)
            return response
        bad_request = HttpResponse('Not a valid action')
        bad_request.status_code = 400
        return bad_request

    def game(self, request, **kwargs):
        # now = datetime.datetime.now()
        now = timezone.now()

        first_game_groups = Game.objects.filter(group=Game.GROUP_CHOICES[0][0]).order_by('gametime').first()
        first_game_main = Game.objects.filter(group=Game.GROUP_CHOICES[1][0]).order_by('gametime').first()

        gameid = request.POST.get('game')

        game = Game.objects.get(id=gameid)
        if (
            (first_game_groups and game.group == Game.GROUP_CHOICES[0][0] and first_game_groups.gametime < now) or
            (first_game_main and game.group == Game.GROUP_CHOICES[1][0] and first_game_main.gametime < now)
        ):
            return JsonResponse({'success': False, 'error_message': 'Game has already started'})

        instance, created = UserGuess.objects.get_or_create(game=game, user=request.user)
        instance.home_score = request.POST.get('homescore')
        instance.away_score = request.POST.get('awayscore')
        instance.save()
        return JsonResponse({'success': True})

    def extra(self, request, **kwargs):
        now = timezone.now()

        extraid = request.POST.get('extra')

        guessid = request.POST.get('guess')

        guess = BonusQuestionChoices.objects.get(id=guessid)

        question = BonusQuestion.objects.get(id=extraid)

        firstgame = Game.objects.all().order_by('gametime')[0]
        if firstgame.gametime < now:
            return JsonResponse({'success': False, 'error_message': 'Competition has aldready started'})
        instance, created = UserBonusAnswer.objects.get_or_create(question=question, user=request.user)
        instance.answer = guess
        instance.save()
        return JsonResponse({'success': True})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        competition = Competition.objects.first()
        now = timezone.now()
        first_game_groups = Game.objects.filter(group=Game.GROUP_CHOICES[0][0]).order_by('gametime').first()
        first_game_main = Game.objects.filter(group=Game.GROUP_CHOICES[1][0]).order_by('gametime').first()
        context['games'] = Game.objects.filter(competition=competition).prefetch_related(
            Prefetch('userguess', queryset=UserGuess.objects.filter(user=self.request.user))
        ).order_by('gametime')

        context['extras'] = BonusQuestion.objects.filter(competition=competition).prefetch_related(
            Prefetch('bonusguess', queryset=UserBonusAnswer.objects.filter(user=self.request.user)),
            'bonuschoices'
        )

        context['groupstarted'] = first_game_groups.gametime < now
        context['mainstarted'] = first_game_main.gametime < now if first_game_main is not None else False

        return context


class UserGuessView(TemplateView):
    pass
