from django import forms
from django.forms.formsets import BaseFormSet, formset_factory
from django.http import Http404
from .models import UserGuess, Game


class UserGuessForm(forms.ModelForm):

    class Meta:
        model = UserGuess
        fields = ['game', 'home_score', 'away_score', 'user']

    def __init__(self, **kwargs):
        self.gamename = kwargs.pop('game')
        request = kwargs.pop('request')
        super().__init__(**kwargs)

        self.fields['game'].widget = forms.HiddenInput()
        self.fields['game'].initial = self.gamename

        self.fields['user'].widget = forms.HiddenInput()
        self.fields['user'].initial = request.user


class BaseUserGuessFormSet(BaseFormSet):

    def __init__(self, **kwargs):
        self.competition = kwargs.pop('competition')
        self.request = kwargs.pop('request')
        self.games = Game.objects.filter(competition=self.competition)
        self.userguess = {i.game.id: i for i in UserGuess.objects.filter(user=self.request.user)}
        self.extra = self.games.count()

        if not self.extra:
            raise Http404('Competition has no games')

        super().__init__(**kwargs)

    def _construct_form(self, index, **kwargs):
        kwargs['game'] = self.games[index]
        kwargs['request'] = self.request
        userguess = self.userguess.get(self.games[index].id)
        if userguess is not None:
            kwargs['initial'] = userguess.__dict__
        return super()._construct_form(index, **kwargs)


UserGuessFormSet = formset_factory(UserGuessForm, formset=BaseUserGuessFormSet)
