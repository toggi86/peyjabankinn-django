# Generated by Django 2.1.4 on 2019-01-06 02:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('peyjabanki', '0015_game_group'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userbonusanswer',
            name='answer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bonusguess', to='peyjabanki.BonusQuestionChoices'),
        ),
    ]
